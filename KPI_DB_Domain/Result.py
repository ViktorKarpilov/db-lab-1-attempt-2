class Result():
    def __init__(this, subject, is_passed, score100, score12, score, scale, ptn_name, ptr_name, pta_name, ptt_name):
        this.Subject = subject
        this.IsPassed = is_passed
        this.Score100 = score100
        this.Score12 = score12
        this.Score = score
        this.Scale = scale
        this.PTNName = ptn_name
        this.PTRName = ptr_name
        this.PTAName = pta_name
        this.PTTName = ptt_name