class Person():
    def __init__(this, identifier, birth, sex, reg_name, area_name, ter_name,
                       reg_type, ter_type, class_prof, class_lang, eo_name,
                       eo_type, eo_reg, eo_area, eo_ter, eo_parent):
        this.Identifier = identifier
        this.Results = []
        this.Birth = birth
        this.Sex = sex
        this.RegName = reg_name
        this.AreaName = area_name
        this.TerName = ter_name
        this.RegType = reg_type
        this.TerType = ter_type
        this.ClassProf = class_prof
        this.ClassLang = class_lang
        this.EoName = eo_name
        this.EOType = eo_type
        this.EOReg = eo_reg
        this.EOArea = eo_area
        this.EOTer = eo_ter
        this.EOParent = eo_parent
